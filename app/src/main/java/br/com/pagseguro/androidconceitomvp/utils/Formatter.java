package br.com.pagseguro.androidconceitomvp.utils;


import java.math.BigDecimal;
import java.util.Locale;

public class Formatter {
    public static String toCurrency(BigDecimal amount) {
        return String.format(new Locale("pt-BR"), "R$ %.2f", amount);
    }
}
