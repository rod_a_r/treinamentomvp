package br.com.pagseguro.androidconceitomvp.facade;


import br.com.pagseguro.androidconceitomvp.models.Account;
import br.com.pagseguro.androidconceitomvp.models.UserSession;
import rx.Observable;

public interface ApiFacade {

    Observable<Account> loadAccount(UserSession credentials);

}
