package br.com.pagseguro.androidconceitomvp;

import com.hannesdorfmann.mosby.mvp.MvpView;

import br.com.pagseguro.androidconceitomvp.models.Account;

public interface MainContract extends MvpView {
    void bind(Account account, String greeting);

    void enablePaymentFeature();

    void disablePaymentFeature();
}
