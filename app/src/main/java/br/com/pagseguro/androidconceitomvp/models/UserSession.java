package br.com.pagseguro.androidconceitomvp.models;


public class UserSession {
    private String mToken;

    public UserSession(String token) {
        mToken = token;
    }

    public String getToken() {
        return mToken;
    }
}
