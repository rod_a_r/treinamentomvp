package br.com.pagseguro.androidconceitomvp.business;

import java.math.BigDecimal;

import br.com.pagseguro.androidconceitomvp.models.Account;

public class BalancePaymentFeatureEvaluator implements PaymentFeatureEvaluator {
    @Override
    public boolean doesUserCanPayBills(Account userAccount) {
        return userAccount.getBalance().compareTo(BigDecimal.ZERO) > 0;
    }
}
