package br.com.pagseguro.androidconceitomvp.business;


import java.util.Calendar;

import br.com.pagseguro.androidconceitomvp.models.Account;

public class DayTimeGreetingDescriber implements GreetingDescriber {
    @Override
    public String describe(Account userAccount) {
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if (hour >= 19 || hour < 4) {
            return "Boa noite";
        } else if (hour < 12) {
            return "Bom dia";
        }
        return "Boa tarde";
    }
}
