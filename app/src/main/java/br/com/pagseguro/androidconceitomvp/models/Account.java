package br.com.pagseguro.androidconceitomvp.models;

import java.math.BigDecimal;

public class Account {
    private final String mCustomerName;
    private final BigDecimal mBalance;

    public Account(String customerName, BigDecimal balance) {
        mCustomerName = customerName;
        mBalance = balance;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public BigDecimal getBalance() {
        return mBalance;
    }
}
