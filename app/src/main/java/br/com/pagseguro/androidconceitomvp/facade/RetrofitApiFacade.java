package br.com.pagseguro.androidconceitomvp.facade;


import br.com.pagseguro.androidconceitomvp.AccountVO;
import br.com.pagseguro.androidconceitomvp.Api;
import br.com.pagseguro.androidconceitomvp.models.Account;
import br.com.pagseguro.androidconceitomvp.models.UserSession;
import rx.Observable;
import rx.functions.Func1;

public class RetrofitApiFacade implements ApiFacade {

    private Api mApi;

    public RetrofitApiFacade(Api api) {
        mApi = api;
    }

    @Override
    public Observable<Account> loadAccount(UserSession session) {
        return mApi.getAccountInfo(session.getToken())
                .map(toAccount());
    }

    private Func1<? super AccountVO, ? extends Account> toAccount() {
        return new Func1<AccountVO, Account>() {
            @Override
            public Account call(AccountVO accountVO) {
                return new Account(accountVO.customerName(), accountVO.balance());
            }
        };
    }


}
