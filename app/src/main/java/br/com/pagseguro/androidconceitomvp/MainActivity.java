package br.com.pagseguro.androidconceitomvp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpActivity;

import br.com.pagseguro.androidconceitomvp.injection.DaggerInjector;
import br.com.pagseguro.androidconceitomvp.injection.Injector;
import br.com.pagseguro.androidconceitomvp.injection.SimpleModule;
import br.com.pagseguro.androidconceitomvp.models.Account;
import br.com.pagseguro.androidconceitomvp.utils.Formatter;

public class MainActivity extends MvpActivity<MainContract, MainPresenter> implements MainContract {

    TextView mLabelCustomerName;
    TextView mLabelBalanceAmount;
    TextView mLabelGreeting;
    Button mButtonPayBill;
    private Injector mInjector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mInjector = DaggerInjector.builder()
                .simpleModule(new SimpleModule())
                .build();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpUI();
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return mInjector.presenter();
    }

    private void setUpUI() {
        mButtonPayBill = (Button) findViewById(R.id.button_payment);
        mLabelCustomerName = (TextView) findViewById(R.id.label_customer);
        mLabelGreeting = (TextView) findViewById(R.id.label_greeting);
        mLabelBalanceAmount = (TextView) findViewById(R.id.label_balance);
        mButtonPayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payBill();
            }
        });
    }

    private void payBill() {

    }

    @Override
    public void bind(Account account, String greeting) {
        mLabelCustomerName.setText(account.getCustomerName());
        mLabelGreeting.setText(greeting);
        mLabelBalanceAmount.setText(Formatter.toCurrency(account.getBalance()));
    }

    @Override
    public void enablePaymentFeature() {
        mButtonPayBill.setVisibility(View.VISIBLE);
    }

    @Override
    public void disablePaymentFeature() {
        mButtonPayBill.setVisibility(View.GONE);
    }
}
