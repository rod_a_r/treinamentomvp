package br.com.pagseguro.androidconceitomvp.injection;

import br.com.pagseguro.androidconceitomvp.Api;
import br.com.pagseguro.androidconceitomvp.business.BalancePaymentFeatureEvaluator;
import br.com.pagseguro.androidconceitomvp.business.DayTimeGreetingDescriber;
import br.com.pagseguro.androidconceitomvp.business.GreetingDescriber;
import br.com.pagseguro.androidconceitomvp.business.PaymentFeatureEvaluator;
import br.com.pagseguro.androidconceitomvp.facade.ApiFacade;
import br.com.pagseguro.androidconceitomvp.facade.RetrofitApiFacade;
import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

@Module
public class SimpleModule {

    @Provides
    public Api providesApi() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://5s6vd4kupsraczxxp-mock.stoplight-proxy.io/")
                .build();
        return adapter.create(Api.class);
    }

    @Provides
    public ApiFacade providesApiFacade(Api api) {
        return new RetrofitApiFacade(api);
    }

    @Provides
    public GreetingDescriber providesGreetingDescriber() {
        return new DayTimeGreetingDescriber();
    }

    @Provides
    public PaymentFeatureEvaluator providesPaymentFeatureEvaluator() {
        return new BalancePaymentFeatureEvaluator();
    }

}
