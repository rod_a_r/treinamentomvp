package br.com.pagseguro.androidconceitomvp.injection;

import br.com.pagseguro.androidconceitomvp.MainPresenter;
import dagger.Component;

@Component(modules = SimpleModule.class)
public interface Injector {

    MainPresenter presenter();

}
