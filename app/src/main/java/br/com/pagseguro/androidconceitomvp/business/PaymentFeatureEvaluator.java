package br.com.pagseguro.androidconceitomvp.business;


import br.com.pagseguro.androidconceitomvp.models.Account;

public interface PaymentFeatureEvaluator {

    boolean doesUserCanPayBills(Account userAccount);

}
