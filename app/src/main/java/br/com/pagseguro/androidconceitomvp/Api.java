package br.com.pagseguro.androidconceitomvp;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface Api {

    @GET("/account")
    Observable<AccountVO> getAccountInfo(@Query("token") String token);

}
