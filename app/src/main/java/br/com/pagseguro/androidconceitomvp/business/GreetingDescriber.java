package br.com.pagseguro.androidconceitomvp.business;


import br.com.pagseguro.androidconceitomvp.models.Account;

public interface GreetingDescriber {
    String describe(Account userAccount);
}
