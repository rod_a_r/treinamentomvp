package br.com.pagseguro.androidconceitomvp;

import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;

import javax.inject.Inject;

import br.com.pagseguro.androidconceitomvp.business.GreetingDescriber;
import br.com.pagseguro.androidconceitomvp.business.PaymentFeatureEvaluator;
import br.com.pagseguro.androidconceitomvp.facade.ApiFacade;
import br.com.pagseguro.androidconceitomvp.models.Account;
import br.com.pagseguro.androidconceitomvp.models.UserSession;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainPresenter extends MvpNullObjectBasePresenter<MainContract> {
    UserSession mUserSession1 = new UserSession("1");
    UserSession mUserSession2 = new UserSession("2");

    private final PaymentFeatureEvaluator mPaymentEvaluator;
    private final GreetingDescriber mGreetingDescriber;
    private final ApiFacade mApiFacade;

    @Inject
    public MainPresenter(PaymentFeatureEvaluator paymentEvaluator,
                         GreetingDescriber greetingDescriber, ApiFacade apiFacade) {
        mPaymentEvaluator = paymentEvaluator;
        mGreetingDescriber = greetingDescriber;
        mApiFacade = apiFacade;
    }

    @Override
    public void attachView(MainContract view) {
        super.attachView(view);
        retrieveAccountInfo();
    }

    private void retrieveAccountInfo() {
        mApiFacade.loadAccount(mUserSession2)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Account>() {
                    @Override
                    public void call(Account account) {
                        getView().bind(account, mGreetingDescriber.describe(account));
                        if (mPaymentEvaluator.doesUserCanPayBills(account)) {
                            getView().enablePaymentFeature();
                        } else {
                            getView().disablePaymentFeature();
                        }
                    }
                });
    }
}
